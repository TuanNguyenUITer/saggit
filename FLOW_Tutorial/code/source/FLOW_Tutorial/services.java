package FLOW_Tutorial;

// -----( IS Java Code Template v1.2

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
// --- <<IS-END-IMPORTS>> ---

public final class services

{
	// ---( internal utility methods )---

	final static services _instance = new services();

	static services _newInstance() { return new services(); }

	static services _cast(Object o) { return (services)o; }

	// ---( server methods )---




	public static final void javaSv (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(javaSv)>> ---
		// @sigtype java 3.5
		// [i] field:1:required d
		// [o] field:1:required ff
		// pipeline
		
		// pipeline
		IDataCursor pipelineCursor = pipeline.getCursor();
			String[]	d = IDataUtil.getStringArray( pipelineCursor, "d" );
		pipelineCursor.destroy();
		
		// pipeline
		IDataCursor pipelineCursor_1 = pipeline.getCursor();
		String[]	ff = new String[1];
		ff[0] = "ff";
		IDataUtil.put( pipelineCursor_1, "ff", ff );
		pipelineCursor_1.destroy();
		// --- <<IS-END>> ---

                
	}
}

